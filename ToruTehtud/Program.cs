﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToruTehtud
{

    //static class E
    //{
    //    public static IEnumerable<int> Paaris (this IEnumerable<int> m)
    //    {
    //        foreach (var x in m) // foreach-iga läbi käies saadakse int-id, vastuse tüüp ka int
    //            if (x % 2 == 0) yield return x; // kui näed nende seas arvu mis on võimalik jagada 2, jaga 2, kui leiad selle siis anna see tagasi
    //    }

    //    public static IEnumerable<int> Ruudud(this IEnumerable<int> m)
    //    {
    //        foreach (var x in m)
    //            yield return x * x;
    //    }
    //    public static IEnumerable<int> Suuremad(this IEnumerable<int> m, int v)
    //    {
    //        foreach (var x in m)
    //            if ( x > v ) yield return x;
    //    }

    //    public static IEnumerable<int> Kas(this IEnumerable<int> m, Func<int, bool> f)
    //    {
    //        foreach (var x in m) if (f(x)) yield return x;
    //    }

    //    public static IEnumerable<int> Arvuta(this IEnumerable<int> m, Func<int, int> f)
    //    { 
    //        foreach (var x in m) yield return f(x);
    //    }

    //}

    class Program
    {

       // static bool KasOnPaaris(int x) => x % 2 == 0;
       // static bool KasOnSuurem(int x, int v) => x > v;


        static void Main(string[] args)
        {
            int[] arvud = { 1, 7, 2, 4, 3, 1, 9, 6, 2, 8, 10, 7 }; 
            foreach ( var x in arvud
               // .Suuremad(5) // kirjutab välja 5 suuremate paarisarvude ruudud
               // .Paaris()
                .Where((int x) => x % 2 == 0) //copytud static boolist.. kas asemel Where ja Select - need asjad on juba olemas ja juba välja mõeldud juba kasutamiseks!
                .Where(x => x > 5)                      //sulgude sees osa on lambda-avaldis - let - lambda kujutatakse C# => 
                .Select(x => x * x) // kirjutab välja paarisarvude ruudud
                ) Console.WriteLine(x);

            string[] nimed = { "Margit", "Ants", "Peeter", "Joosep", "Jakob" };
            foreach(var n in nimed
                   .Where(x => x[0] =='J')
                ) Console.WriteLine(n);

            foreach(var x in from y in arvud                // sama mis ülemine foreach, aga kirjutatud teistmoodi.. 
                               where y > 5 && y % 2 == 0    // siin on kirjutatud .where-d 
                               select y * y)                // siin on .select kirjutatud 

                Console.WriteLine(x);


            Random r = new Random();

            Console.WriteLine("\nLambdad spordipäeval\n");
            foreach (var x in
                System.IO.File.ReadAllLines(@"C:\Users\margi\Source\Repos\march-2020-second-week\ToruTehtud\spordipäeva protokoll.txt")
                .Skip(1) // .skip .take
                //.Where(x => ref.Next(5) == 1)
                .Select(x => x.Split(','))
                .Select(x => new {Nimi = x[0], Distants = int.Parse(x[1]), Aeg = double.Parse(x[2])})
                .Where( x => x.Aeg > 0)
                .Select(x => new {x.Nimi, x.Distants, x.Aeg, Kiirus = x.Distants / x.Aeg})
                .OrderBy(x => x.Kiirus) // sorteerib väiksemast suuremaks
                .OrderByDescending(x => x.Kiirus) //sorteerib suuremast väiksemaks kiiruse
               
                .GroupBy(x => x.Distants)
                .Select(x => new {Distants = x.Key, Parim = x.OrderByDescending (y => y.Kiirus).First()})
                ) Console.WriteLine(x);





        }
    }
}
