﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Ylesanne
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Inimene> õpilased = new List<Inimene>();
            try
            {
                foreach (var rida in File.ReadAllLines(@"C:\Users\margi\Source\Repos\march-2020-second-week\Ylesanne\Opilased.txt"))
                {
                    var osad = rida.Replace(", ", ",").Split(',');
                    if (osad.Length > 1) õpilased.Add(new Inimene { Nimi = osad[1], Isikukood = osad[0] });
                }

                foreach (var i in õpilased) Console.WriteLine($"{i} vanus on {i.Vanus()}");
            }
            catch
            {

            }
        }

            /* MINU PUSIMINE - TUNDUB JURA..
            var sisu = File.ReadAllLines(filename);
            Console.WriteLine(string.Join("\n", sisu));

            for (int i = 0; i < sisu.Length; i++)
            {
                var rida = sisu[i].Split(',');
                string isikukood = rida[0];
                string nimi = rida[1];
                string klass = rida[2];

                õpilased.Add(new Inimene
                {
                    Nimi = nimi,
                    Isikukood = isikukood,
                    Klass = klass
                });


            } */

     

            //static int Vanus(DateTime päev)
            //{
            //    var v = (DateTime.Today - päev).Days * 4 / 1461;
            //    return v;
            //}

        class Inimene
        {
            public string Nimi;
            public string Isikukood;

            //tahame sünniaega ja vanust - vanust oskame, sünniajaga keeruline

            public DateTime Sünniaeg()
            {
                int sajand = 1800;
                switch (Isikukood.Substring(0, 1))
                {
                    case "3": case "4": sajand = 1900;
                        break;
                    case "5": case "6": sajand = 2000;
                        break;
                }


                return new DateTime(
                   int.Parse(Isikukood.Substring(1,2)),
                   int.Parse(Isikukood.Substring(3, 2)),
                   int.Parse(Isikukood.Substring(5, 2))
                   );

            }
            public int Vanus() => (DateTime.Today - Sünniaeg()).Days * 4 / 1461;

            public override string ToString()
            => $"{Isikukood} - {Nimi}";



        }

    }
}
