﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KleineIntermedium
{
    class Program
    {
        static void Main(string[] args)
        {
            // PalgaArvestus.Maksuvaba = 1000; // kui see ees, kirjutab vastuseks 0
            Console.WriteLine(Inimene.Maks(800)); // kirjutab vastuse 60,0 (ehk 800-500 = 300 -> 300st 20%)

            List<Inimene> inimesed = new List<Inimene>
            {
                new Inimene {Nimi = "Margit", Palk = 10000},
                new Inimene {Nimi = "Ants", Palk = 800},
                new Inimene {Nimi = "Peeter", Palk = 100}

            };

            foreach (var i in inimesed) Console.WriteLine($"{i.Nimi} saab {i.Palk - i.Tulumaks()} raha");

        }
    }

    class Inimene // classi ees static EI VABASTA MEID
    {
        public static decimal Maksuvaba { get; set; } = 500M; // need on ühised kõikidel inimestel, seega ees on static
        public static decimal Maksumäär { get; set; } = 0.2M; // siin peab lõppu kirjutama M sest on decimal, muidu arvaks, et on static 

        //public string Nimi { get; set; } // ei ole static ees, sest igal inimesel on oma nimi ja palk
        //public decimal Palk { get; set; }

        //public decimal Tulumaks => Palk < Maksuvaba ? 0 : (Palk - Maksuvaba) * Maksumäär;

        public static decimal Maks(decimal summa) => summa < Maksuvaba ? 0 : (summa - Maksuvaba) * Maksumäär;

    
        public string Nimi { get; set; }
        public decimal Palk { get; set; }

        public decimal Tulumaks() => Maks(Palk);

    }

}
