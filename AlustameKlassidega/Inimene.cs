﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlustameKlassidega
{
    class Inimene 
    {
        // andmed, baidid, bitid, nende tähendus
        public string Nimi; //vaikimisi tühi string
        public int Vanus; //vaikimisi 0 (ümmargune null)
        public Inimene Kaasa; //vaikimisi null (nö kandiline null, selle aasdressi peal ei ole midagi)

        // public override string ToString() => $"Inimene {Nimi} vanusega {Vanus}";

    }
}
