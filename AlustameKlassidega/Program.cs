﻿using MinuLoomad; // lisatud teise faili using siia, et ei läheks sassi! ja muidu ei lase kasutada.. 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlustameKlassidega //klassid jaotatakse nimeruumi, et nimed ei läheks sassi!
{
    class Program //FAILI NIMI PEAB OLEMA SAMA MIS CLASSIL! (nagu siin, et ei läheks sassi) // tegelik pikk nimi on class AlustameKlassidega.Program //
    {
        static void Main(string[] args)
        {
            Inimene margit = new Inimene(); // uus tüüp inimene ja selle kaudu tekib uus muutuja. enne ei saa midagi teha kui on antud väärtus.
            margit.Nimi = "Margit Marjamäe"; // inimene koosneb nimest ja vanusest.
            margit.Vanus = 29;

            Inimene ants = new Inimene() { Nimi = "Ants Saunamees", Vanus = 80 };

            List<Inimene> rahvas = new List<Inimene>
            {
                new Inimene {Nimi ="Joosep"}, 
                new Inimene {Nimi ="Teele"},
                new Inimene {Nimi ="Arno"},
                margit,
                ants,
            };
            Console.WriteLine();

            Console.WriteLine(margit);
            // muutuja - mälus on piirkond kus võimalik andmeid hoida, bitidbaidid, võimalikud väärtused, mis võimalik sellega peale hakata..
            // kasutatakses mudelit - lihtsustatud kujutis

            Koer koer = new Koer();

            //Imelik imelik = new Imelik();

            Inimene teine = margit; // omistame muutuja ja avaldis sama tüüpi 
            teine.Nimi = "Margitm";

            Console.WriteLine(margit); // prindib välja Margitm, sest aadress jääb margitil samaks, omistatud teine väärtus.


            // struct erineb class-ist selle poolest, et muutuja sees on väärtus. omistamisel kopeeritakse väärtus.
            // class on reference tüüpi asi, muutujas on aadress
            // struct on value tüüpi asi, muutujas on väärtus

            Inimene[] inimesed = new Inimene[10]; // kümme inimest massiivil
            List<Inimene> teised = new List<Inimene>();

            teised.Add(new Inimene { Nimi = "Ants", Vanus = 28 });
            teised.Add(new Inimene { Nimi = "Ants" });
            teised.Add(margit);
            margit.Kaasa = new Inimene { Nimi = "Kaarel", Kaasa = margit };

            foreach (var x in teised) Console.WriteLine(x.Kaasa?.Nimi ?? "kaasa puudub");


            //SPRDIPÄEVA ÜLESANNE KOOS KLASSIDEGA JA SELGITUSEGA - VT HENNI GITLAB!!


        }


    }


    // 2 references siin tähendab seda, et on kahes kohas kasutatud
    /* - andis erroreid see osa..
     
     class Inimene // uus klass, inimene - nimi,vanus // kirjeldame ära millest koosneb inimene - siin nimi ja vanus.
                  //classide järjekord kasutamisel ei ole oluline !! definitsioonide järjekord ei oma tähtsust


    {
        public string Nimi;
        public int Vanus;

        public override string ToString() => $"Inimene {Nimi} vanusega {Vanus}";
    }
    */


}
