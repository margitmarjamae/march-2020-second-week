﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassideTuletamine // vaata videot ja täienda! 09.03.2020 salvestus
{
    class Program
    {
        static void Main(string[] args)
        {
            Loom loom1 = new Loom();
            // Console.WriteLine(loom1);
            Loom loom2 = new Loom("krokodill");
            // Console.WriteLine(loom2);

            Koduloom kl = new Koduloom("prussakas") { Nimi = "Albert" };
            // Console.WriteLine(kl);


        }
    }

    abstract class Elajas //abstraktne meetod tähendab, et seda päriselt olemas ei ole, aga seda saab kasutada. tuleb kirjeldada kuidas seda tehakse..
    {
        public abstract void Söömine(Elajas e);
    }


    class Loom : Elajas
    {
        public string Liik { get; set; }


        //baasklassist ei tule kaasa konstruktorid. 
        //seega koduloomale on vaja teha oma konstruktor 
        // kui klassil ei ole konstruktorit, siis antakse vaikimisi konstruktor, mis ei tee midagi


    }

    class Koduloom : Loom
    {

    }

    class Kass : Koduloom
    {

    }

    // kolm uut tehet
    // is 
    // as
    // ? .

    class Metskass : Kass
    {
        // sealed override - pitseerib kinni nii et seda tuletatud klassis ei saa enam ümber kirjeldada 


    }

    class Koer : Koduloom
    {
        public string Tõug { get; set; }
        public Koer () : base("koer") { }

    }

    interface ISöödav
    {
        void Süüakse();
        void JäetakseSöömata();
        bool KasMaitses();
    }

    class Sepik : ISöödav // ees I - tunneme kaugelt ära, et tegemist interface-ga
    {
        public void JäetakseSöömata()
        {
            throw new NotImplementedException();
        }
    }



}
