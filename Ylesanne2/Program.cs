﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Ylesanne2
{
    static class MyFunctions

    {
        public static int Vanus2(this Inimene p) => (DateTime.Today - p.Sünniaeg).Days * 4 / 1461;

        public static string ToProper(this string s)
            => s == "" ? "" :
               s.Substring(0, 1).ToUpper() + s.Substring(1).ToLower();

        public static string[] ReadAllLines(this string filename)
            => File.ReadAllLines(filename);


        public static int Liida(int a, int b)
        {
            return a + b;
        }

        public static int Ruut(this int x) => x * x;


        public static int Tryki(this int a, string nimi)
        {
            Console.WriteLine($"{nimi}: {a}");
            return a;
        }

        public static void Arvuta(int x, int y, out int summa, out int korrutis)
        {
            summa = x + y;
            korrutis = x * y;
        }

        public static (int, int) Arvuta(int x, int y)
        { 
           return (x + y, x * y);
        }

        public static void Vaheta(ref int x, ref int y)
        {
            //int ajutine = x;
            //x = y;
            //y = ajutine;
            (x, y) = (y, x);
        }

        //public static void Vaheta(ref string x, ref string y) => (x, y) = (y, x);

        public static void Swap<T>(ref T x, ref T y) => (x, y) = (y, x); // osab vahetada misiganes asju!! mitte ainult int ja string, ka nt DateTime!!


        public static int Factorial(int x)
            => x < 2 ? 1 : x * Factorial(x - 1);    //rekursiivsed funktsioonid

        public static int Fact(int x)       //teine faktoriaali näide
        {
            int vastus = 1;
            while (x > 1) vastus *= x--;
            return vastus;
        }

    }



    class Program
    {
        static void Main(string[] args)
        {

            DateTime minusünnipäev = new DateTime(1990, 9, 20);
            DateTime täna = DateTime.Today;

            MyFunctions.Swap(ref minusünnipäev, ref täna); // vaheta ära minu sünnipäev ja tänane päev
            
            int arv = 77;
            arv.Tryki("arv").Ruut().Tryki("ruut");

            (77.Tryki("seitse").Ruut() + 100.Tryki("lisasin 100")).Tryki("tulemus");

            MyFunctions.Tryki(arv, "arv");
            MyFunctions.Tryki(MyFunctions.Liida(7, 4), "tulemus");

            int s, k;
            MyFunctions.Arvuta(7, 5, out s, out k);
            MyFunctions.Swap(ref s, ref k);

            MyFunctions.Vaheta(ref k, ref s);

            Inimene margit = new Inimene { Isikukood = "49009205216", Nimi = "Margit" };          //katsetamiseks
            Console.WriteLine(margit.VanusO());
            Console.WriteLine(MyFunctions.Vanus2(margit));
            Console.WriteLine(margit.Vanus2());

            //Console.WriteLine(margit.Sugu);
            // Console.WriteLine(margit.Vanus);




            //HARJUTUS!

            List<Inimene> Inimesed = new List<Inimene>();
            ReadInimesed(@"C:\Users\margi\source\repos\march-2020-second-week\Ylesanne2\Õpilased.txt", Inimesed);
            ReadInimesed(@"C:\Users\margi\source\repos\march-2020-second-week\Ylesanne2\Õpetajad.txt", Inimesed);
            
            //Järgmise nädala sünnipäevad:

            DateTime startNextWeek = DateTime.Today.AddDays(7 - ((int)(DateTime.Today.DayOfWeek) + 6) % 7); // nii saab teha, et nädal algaks esmaspäevast 
            DateTime stopNextWeek = startNextWeek.AddDays(7);

            Console.WriteLine("\nJärgmise nädala sünnipäevalapsed \n");  
            foreach(var p in Inimesed)
            {
                DateTime sünnipäev = p.Sünniaeg.AddYears(p.Vanus + 1);
                if (sünnipäev >= startNextWeek && sünnipäev < stopNextWeek) Console.WriteLine(p);
            }


            //foreach (var n in Inimesed)
            //    if (n.Sünniaeg.DayOfWeek == DateTime.Today.DayOfWeek + 7)
            //        Console.WriteLine(n);


            Console.WriteLine("\nJärgmise kuu sünnipäevalapsed \n");
            foreach (var p in Inimesed)
                if (p.Sünniaeg.Month == DateTime.Now.Month + 1) // järgmisel kuul sünnipäev
                    Console.WriteLine(p);

            #region Minu pusimine 

            /*
            string filename = @"C:\Users\margi\source\repos\march-2020-second-week\Ylesanne2\Õpilased.txt";
            string filename2 = @"C:\Users\margi\source\repos\march-2020-second-week\Ylesanne2\Õpetajad.txt";

            List<Inimene> finalData = new List<Inimene>();

            foreach (var rida in (File.ReadAllLines(filename)).Concat(File.ReadAllLines(filename2)))
            {
                var osad = rida.Replace(", ", ",").Split(',');
                if (osad.Length > 0) finalData.Add(new Inimene { Nimi = osad[1], Isikukood = osad[0] });
            }

            // foreach (var i in finalData) Console.WriteLine($"{i} vanus on {i.Vanus()}");
            */

            #endregion

        }

        static void ReadInimesed(string filename, List<Inimene> list)
        {
            foreach (var rida in (File.ReadAllLines(filename)))
            {
                var osad = rida.Split(',');
                if (osad.Length > 1)
                list.Add(new Inimene { Isikukood = osad[0].Trim(), Nimi = osad[1].Trim() });
            }
        }
    }
}
