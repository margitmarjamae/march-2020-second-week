﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Harjutus11KlaasideTuletamine
{
    class Program
    {
        static void Main(string[] args)
        {
            //testimiseks:
            //Console.WriteLine(new Inimene { Nimi = "Margit Marjamäe", IK = "49009205216" });
            //Console.WriteLine(new Õpilane { Nimi = "Toomas Linnupoeg", IK = "39005210000", Klass = "7C" });
            //Console.WriteLine(new Õpetaja { Nimi = "Malle Maasikas", IK = "46503010000", Aine = "Matemaatika" });
            //Console.WriteLine(new Õpetaja { Nimi = "Tiit Tuvi", IK = "35306010000", Aine = "Füüsika", Klass = "7C" });

            List<Inimene> koolipere = new List<Inimene>
            {
                new Inimene { Nimi = "Margit Marjamäe", IK = "49009205216" },
                new Õpilane { Nimi = "Toomas Linnupoeg", IK = "39005210000", Klass = "7C" },
                new Õpetaja { Nimi = "Malle Maasikas", IK = "46503010000", Aine = "Matemaatika" },
                new Õpetaja { Nimi = "Tiit Tuvi", IK = "35306010000", Aine = "Füüsika", Klass = "7C" }
            };

            var õpsid = File.ReadAllLines(@"C:\Users\margi\Source\Repos\march-2020-second-week\Harjutus11KlaasideTuletamine\Õpetajad.txt");

            foreach (var õ in õpsid)
            {
                string [] osad = (õ + ",,").Split(',');
                koolipere.Add(new Õpetaja { IK = osad[0].Trim(), Nimi = osad[1].Trim(), Aine = osad[2].Trim(), Klass = osad[3].Trim() });
            }


            õpsid = File.ReadAllLines(@"C:\Users\margi\Source\Repos\march-2020-second-week\Harjutus11KlaasideTuletamine\Õpilased.txt");

            foreach (var õ in õpsid)
            {
                string[] osad = (õ + ",,").Split(',');
                koolipere.Add(new Õpilane { IK = osad[0].Trim(), Nimi = osad[1].Trim(), Klass = osad[2].Trim() });
            }

            foreach (var x in koolipere) Console.WriteLine(x);

        }

    }

    enum Sugu { Naine, Mees }

    class Inimene
    {
        public string Nimi { get; set; }
        public string IK { get; set; }

        public Sugu Sugu => (Sugu)(IK[0] % 2);

        public override string ToString() => $"{Sugu} {Nimi} ({IK})";

    }

   

    class Õpilane : Inimene
    {
        public string Klass { get; set; }

        public override string ToString() => $"{Klass} klassi õpilane { Nimi } ({IK})";
    }

    class Õpetaja : Inimene
    {
        public string Aine { get; set; }
        public string Klass { get; set; } = ""; // klass mida juhatab 

        public override string ToString() => $"{Aine} õpetaja {Nimi} ({IK})" + 
            ( Klass == "" ? "" : $" / {Klass} klassi juhataja");

    }
}
