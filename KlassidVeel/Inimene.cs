﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassidVeel
{
    class Inimene // peetri näiteks jaoks muutsime class -> struct-iks, siis sai kasutada peetri näites, vt Program-ist, muutsin tagasi
                  // klassisüsteemis on teistmoodi võivad olla definitsioonid suvalises järjekorras - see on definitsioonide kogu
                  // klassis ei saa teha avaldisi suunatud teistele definitsioonidele! 

    {
        static int loendur = 0;
        static List<Inimene> inimesed = new List<Inimene>();

        public static IEnumerable<Inimene> Inimesed => inimesed.AsEnumerable();           //siis jätab nimed kõik alles, GC ei lase kustutada

        private string _Nimi = ""; 
        public string Nimi
        {
            get => _Nimi;

            set => _Nimi = ToProper(value); // nime kirjutamisel tehakse see alati suure algustähega, vahet ei ole kas get või set juures 
        }




        //public string Nimi = "nimetu";
        public string IK;
        //int _NR = ++loendur; // seda saab kirjutada, sest eelnevalt defineeritud loendur //NR-number // public eest kustutatud, siis tegemist PRIVATE-ga! 

        //uus variant eelnevast.... 
        //private string _nimi;
        //public string Nimi { get => _nimi; set => _nimi = value; }
        //public string IK { get, private set; } = "00000000000";
        //public int Nr { get; } = ++loendur;

        private decimal _Palk; // _Nimi - on nö "nööriga kaamel" // privaatne

        public decimal GetPalk() // saab küsida, mis on palk // getter-funktsioon
        {
            return _Palk; 
        }

        public void SetPalk(decimal palk) // setter-funktsioon
           // public void SetPalk(decimal palk) => _Palk > _Palk ? palk : _Palk; saab ka lühemalt ühel real kirja panna alljärgneva if osa
        {
            if (palk > _Palk) _Palk = palk;
        }

        //VEEL ÜKS VARIANT // JAVAS on ainult  property - nõuab get ja set paare

            // property - setter-getter paar, mis näeb välja nagu field
            public decimal Palk
        {
            //pikalt 
            get
            {
                return _Palk;
            }
            set
            {
                _Palk = value > _Palk ? value : _Palk;
            }
            //lühikeselt, töötavad täpselt samamoodi
            // get => _Palk;
            // set => _Palk = value > _Palk ? value : _Palk;
        }


        public Inimene()            // see on CONSTRUCTOR (UUS ASI) - meetod objekti loomiseks 
                                    // käivitatakse automaatselt siis kui käivitatakse new  
                                    // voidi ei ole vahel, klassi nimega sama nimi    
                                    // käivitatakse new ajal // pannakse äsjaloodud new-ga inimene listi kirja
        {
            /*this.*/
            Nimi = "";         // kuni nimedega ei ole segadust, ei tule seda this. ette lisada
            /*this.*/
            IK = "00000000000";

            inimesed.Add(this);     // spetsiaalne muutuja this - märgib seda uut lisatud inimest 
        }


        public DateTime ilus = DateTime.Now; // selline asi on lubatud 

        public DateTime Sünniaeg() // sulud ära kustutades muutub automaatselt read onlyks - return muuta get =>
        {
            return new DateTime(
                (IK[0] == '1' || IK[0] == '2' ? 1800 :
                IK[0] == '3' || IK[0] == '4' ? 1900 : 2000) +
                int.Parse(IK.Substring(1, 2)),
                int.Parse(IK.Substring(3, 2)),
                int.Parse(IK.Substring(5, 2))
            );
        }

        public override string ToString() //override-ga teen tuletatud klassis teistmoodi.. ? 05.03. teema
        {
            return $"{Nimi} sündinud {Sünniaeg():dd.MMMM.yyyy}"; //MM - kirjutab .09. // MMMM - kirjutab pikalt september
        }

        public static string ToProper(string nimi)
        {
            var nimed = nimi.Replace("-", "- ").Split(' '); // tükeldamiseks

            for (int i = 0; i < nimed.Length; i++)
            {
                nimed[i] = nimed[i] == "" ? "" : // nüüd peaks tühja nimega toimima ka
                nimed[i].Substring(0, 1).ToUpper() + nimed[i].Substring(1).ToLower();
            }

            return string.Join(" ", nimed).Replace("- ", "-"); // kokku panemiseks
            

            // kuidas teha nimi suure algustähega
            //nimi == "" + "" :       // nüüd peaks tühjaga ka toimima
            //return nimi.Substring(0, 1).ToUpper() + nimi.Substring(1).ToLower();

        }


    }
}
