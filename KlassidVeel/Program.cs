﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KlassidVeel
{
    enum Mast { Risti = 0, Ruutu, Ärtu, Poti } // uus andmetüüp, millel on 4 võimalikku väärtust, neile on antud nimed, et nad tähendaksid midagi
                                               // tegemist on arvuga, saab teha arvuga sarnaseid tehteid
                                               
        // DayOfWeek // see on ka enum!

        [Flags]
        enum Tunnus { Suur = 1, Punane = 2, Puust = 4, Kolisev = 8}


    static class Program // panna ette kirja static, et kuskil ära ei ununeks ja ei hakkaks segama..
    {
        static void Main()
        {
            Console.WriteLine((Tunnus)7);
            Tunnus t = Tunnus.Suur | Tunnus.Kolisev;
            t |= Tunnus.Punane;
            Console.WriteLine(t);

            Mast m = Mast.Risti;
            m++; // prindib väja ruutu
            Console.WriteLine(m); // prindib välja risti
            Console.WriteLine((int)m); 
            Console.WriteLine((Mast)3); // prindib välja ärtu
            
            
            Inimene margit = new Inimene { Nimi = "Margit" };
            margit.SetPalk(10000);
            Console.WriteLine(margit.GetPalk());  //pöördumine nagu meetodi poole
            margit.SetPalk(5000);               //palk väiksemaks ei saa minna
            Console.WriteLine(margit.GetPalk());

            Console.WriteLine(margit.Palk);
            margit.Palk = margit.Palk + 500;
        }



        static void MainVana()
        {
            //tehtud uus Main, et eelmist ei peaks kustutama

            Console.WriteLine(Inimene.ToProper("margit"));
            Console.WriteLine(Inimene.ToProper(" "));
            Console.WriteLine(Inimene.ToProper("jorch-adniel kiir"));
            //Console.WriteLine(Inimene.ToProper(" ")); // ülesanne lahendamata !! 
            //Console.WriteLine(Inimene.ToProper(" "));



            // Console.WriteLine(Inimene.ToProper("o'brien")); - nii ei proovi, läheb segaseks.. 


        }
        

        // vana Maini nimetasime ringi
        static void MainEelmine (string[] args)
        {
            Inimene margit = new Inimene { Nimi = "Margit Marjamäe", IK = "49009205216" };
            Console.WriteLine(margit);          // Console.WriteLine(margit.Sünniaeg()); // (margit); kui toString on kirjutatud koos {return ... $"..."}  

            foreach (var i in Inimene.Inimesed) Console.WriteLine(i);
            Console.WriteLine(Liida(4, 7)); // positsiooniline pöördumine
            // Console.WriteLine(Liida(x : 4, z : 7)); // nimeline pöördumine
            Console.WriteLine(LiidaX(1));
            Console.WriteLine(LiidaX(1, 2, 3, 4, 5, 6, 7)); // liidab kokku selliselt ette antud arvud

            // näide 
            // margit = new Inimene { Nimi = "Uus Margit Marjamäe" }; // siis kaob eelmine margit ära ja sellega enam ei tegeleta, hakkab kasutama viimasena lisatud new Inimest

            //Inimene peeter; // näide classi asemel structi kasutamisest
            //peeter.Nimi = "Peeter";
            //peeter.IK = "10101010000";


            /* 
             * sellest praegu ei räägi... millalgi tulevikus? 
            
            using (Inimene ajutine = new Inimene { Nimi = "Keegi")
            {
                // siin selles blokis on inimene olemas
            }
                //siit alates seda muutujat enam ei ole
            */
        }



        static int Liida(int x, int y)  //pääseb ainult staatiliste asjade kallale // kolme arvu ei saa liita!
        {
            return x + y;
        }
        // 2 või enam funktsiooni sama nimelist funktsiooni, aga erineva parameetrite arvu või tüübiga - funktsioonide ülelaadimine (overload)

        static int Liida(int x, int y, int z = 0)   // z = 0 on juba väärtus antud, optional parameeter, peab olema lõpus, nt x = 1 ei saa algusesse panna
        {
            return x + y + z;
        }
        //static int Liida(int x, int y = 0, int z = 0)   // z = 0 on juba väärtus antud, optional parameeter, peab olema lõpus, nt x = 1 ei saa algusesse panna
        //{
        //    return 4*x + 2*y + z;
        //}

        static double Liida(double d1, double d2)
        {
            return d1 + d2;
        }

        static int LiidaX(int x, params int[] muud) // params annab ette parameetri ülevalt cw realt // muutuva hulgaga parameetreid, sama tüüpi peavad olema
        {
            int sum = x;
            foreach (var s in muud) sum += s;
            return sum;
        }



    }
}
