﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TryCatchDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ma tahaks jagada");

            try
            {
                Console.Write("anna üks arv: ");
                int yks = int.Parse(Console.ReadLine());

                Console.Write("anna teine arv: ");
                int teine = int.Parse(Console.ReadLine());

                if (teine == 0) throw new Exception("Nulliga meie jagada ei oska"); // saa ise välja mõelda ja kirjutada exceptioni - vea puhul 
                // throw new exception tuleb kirja panna enne jagatist!! muidu tekib loogikaviga
                int tulemus = yks / teine;

                Console.WriteLine($"tulemus {tulemus}");
            }
            catch (Exception e)
            {
                Console.WriteLine("midagi juhtus, täna ei jagu");
                Console.WriteLine(e.Message);
                // throw; // kui see lisada siia vahele - annab veateate ja lõpetab tegevuse
            }  
            finally
            {
                Console.WriteLine("Tänaseks lõpetame jagamise");
            }
        }
    }
}
