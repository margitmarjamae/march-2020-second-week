﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeetodidJaFunktsioonid
{
    class Program // kõik funktsioonid ja meetodid paiknevad classi sees - samas ei ole vahet, kus klassi sees need asuvad nende juures mängivad rolli kaks sõna
                  // static - 
                  // public - class member access level (üks neljast) - iga klassiliikme ees! 
                  // 1. public - igalt poolt kätte saadav
                  // (2. protected - kätte saadav oma klassi ja sellest klassist tuletatud uute klasside sees)
                  // 3. internal - kättesaadav kõikides programmides, mis on selle programmi sees
                  // 4. private - kättesaadav ainult selle klassi sees

    // classi liikmed, kui ei ole öeldud, siis on vaikimisi private 
    // class ja struct ise on vaikimisi internal

    {
        static void Main(string[] args)
        //static void - meetod
       
 #region PEITU PRAEGUSEKS
        {
            DateTime sünnipäev = new DateTime(1990, 9, 20);
            // var vanus = (DateTime.Today - sünnipäev).Days * 4 / 1461;

            Trüki(sünnipäev);

            Console.WriteLine(Vanus(sünnipäev)); // annab ette parameetrid (siin sünnipäev), mis saab funtktsiooni parameetri esilekutsumisel // saab küsida kumb vanem jne.. 


            if (Vanus(sünnipäev) > 18) Console.WriteLine("talle võib alkot müüa");
            // läheb uuesti arvutama - läheb uuesti funktsiooni - võrdleb kas suurem kui 18, kui false, siis lauset ei kirjutata
        }


        //funktsiooni lihtsustamise teema
        //static int - funktsioon
        static int Vanus(DateTime päev) // parameeter, muutuja ploki päises sulgudes (mis saab väärtuse kui välja kutsutakse f-n). päev on parameeter-muutuja
                                        // int on siin vastuse tüüp ehk return tüüp 
        {
            var v = (DateTime.Today - päev).Days * 4 / 1461; // siin arvutatakse ja
            return v; // vastuse lause, mis peab olema return tüüpi (siin int)

            // parameetrid - on siis ajutised muutujad, mis saavad väärtuse, kui funktsiooni poole pöördutakse (piiramatu hulk, aga peaks olema MÕISTUSE piires) 


        }


        // see jutt tuleb pärast ära unustada ?? kui üks avaldis siis on lubatud nii teha, hea komme on, et nii ei tee!
        //static int Vanus(DateTime päev) 
        //=> return (DateTime.Today - päev).Days * 4 / 1461; //lambda-avaldis - valesti kirjutatud!!!!!

        static void Trüki(DateTime päev) //meetod
        {
            Console.WriteLine(päev.ToString("(ddd) dd.MMMM.yyyy"));
        }


        #endregion
        
        Inimene margit = new Inimene { Nimi = "Margit", Sünniaeg = new DateTime(1990, 9, 20) };
        Inimene ants = new Inimene { Nimi = "Ants", Sünniaeg = DateTime.Parse("17.aprill 2000") };

        Console.WriteLine(margit);
        Console.WriteLine(ants);

            Console.WriteLine(margit.AgeInstants()); // 

            Console.WriteLine(Inimene.AgeStatic(ants)); // tahan agestatic funktsiooni ja annan ette parameetriks ants ja arvutatakse vanus
            

    }

    class Inimene
    {

        static int inimesteArv = 0;
        static int täisiga = 18;

        public int Number = ++inimesteArv;
        public string Nimi;
        public DateTime Sünniaeg;


        public bool KasTäisealine() { return AgeInstants() > adult; }

        public override string ToString()
        => $"{Number}. {Nimi} kes on sündinud {Sünniaeg:dd.MMMM.yyyy}";




        // margit.AgeInstants()
        public int AgeInstants() => Vanus(this.Sünniaeg); // kutsutakse välja muutuja juurest

        // Inimene.AgeStatic (ants)
        public static int AgeStatic(Inimene kes) => Vanus(kes.Sünniaeg); // kutsutaks välja klassi juurest



        //public string Nimi;
        //public DateTime Sünniaeg;

        internal static int Vanus(DateTime päev) 

    }

    class HeaInimene : Inimene // tuletatud klassist Inimene
    {

    }

}
