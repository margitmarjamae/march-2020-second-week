﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace UusSpordipaevaYlesanne
{
    class Program
    {
        static void Main(string[] args)
        {

            // harjutus, kus jääb küsima, kus fail on nii kaua kuni saab kätte. 
            
            string filename = "spordipäeva protokoll.txt";
            string folder = ".";
            string[] failisisu;
            while (true)
            {
                try
                {
                    failisisu = File.ReadAllLines($"{folder}\\{filename}");
                    break;
                }
                catch (Exception)
                {
                    Console.Write("Kus su fail on: ");
                    folder = Console.ReadLine();
                }
            }

            Console.WriteLine(string.Join("\n", failisisu));


            List<string> vigasedRead = new List<string>();
            List<Tulemus> tulemused = new List<Tulemus>();
            Dictionary<int, Distants> distantsid = new Dictionary<int, Distants>();

            string kiireimJooksja = "";
            double suurimKiirus = 0;


            for (int i = 1; i < failisisu.Length; i++)
            {
                
                try
                {
                    var osad = failisisu[i].Split(',');
                    tulemused.Add(new Tulemus { Nimi = osad[0], Aeg = double.Parse(osad[2]), Distants = double.Parse(osad[1]) });             
                }
                catch (Exception e)
                {
                    vigasedRead.Add($"{i + 1}. {failisisu[i]} # {e.Message}");
                }
            }

            vigasedRead.ForEach(x => Console.WriteLine(x));


            for (int i = 1; i < failisisu.Length; i++)
            {
                var rida = sisu[i].Split(',');
                string nimi = rida[0];
                int distants = int.Parse(rida[1]);
                double aeg = double.Parse(rida[2]);
                double kiirus = distants / aeg;
            }


                tulemused.Add(new Tulemus
            {
                Nimi = nimi,
                Distants = distants,
                Aeg = aeg,
                Kiirus = kiirus
            });

            //teeme kohe kiireima jooksja selgeks
            if (kiirus > suurimKiirus) (suurimKiirus, kiireimJooksja) = (kiirus, nimi);

            if (!distantsid.ContainsKey(distants)) distantsid.Add(distants, new Distants
            {
                Pikkus = distants,
                Parim = nimi,
                Kiirus = kiirus
            });

            if (kiirus > distantsid[distants].Kiirus)
            {
                distantsid[distants].Parim = nimi;
                distantsid[distants].Kiirus = kiirus;
            }
        }

            Console.WriteLine("\nprotokoll\n");
            foreach (var p in tulemused) Console.WriteLine(p);

            Console.WriteLine("\ndistantside parimad\n");
            foreach (var d in distantsid) Console.WriteLine(d.Value);

            Console.WriteLine("\nkiireim jooksja\n");
            Console.WriteLine($"{kiireimJooksja} kiirusega {suurimKiirus:F2}");



            //if (Aeg == 0) throw new Exception("Nulli keskmisel ei arvesta");


        }
    }
    class Tulemus
    {
        public string Nimi { get; set; }
        public double Distants { get; set; }
        public double Aeg { get; set; }
        public double Kiirus => Distants / Aeg;

        public override string ToString() => $"{Nimi} jooksis {Distants} ajaga {Aeg:F2} kiirusega {Kiirus:F2}";

}
    class Distants
    {
    public int Pikkus;
    public double Kiirus;
    public string Parim;

    public override string ToString() => $"distantsi {Pikkus} parim oli {Parim} kiirusega {Kiirus:F2}";
    }