﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Constructorid
{
    class Program
    {
        static void Main(string[] args)
        {
            //new Inimene("49009205216") { Nimi = "Margit" };
            //new Inimene("49009205216", "Margit");

            Inimene.Create("49009205216").Nimi = "Margit";
           

            Console.WriteLine(Inimene.Leia("49009205216") ?. Nimi.ToUpper() ?? "sihukest pole");

            // ühesilmaga elvis 
            Console.WriteLine(
                Inimene.Leia("49009205216") == null ? "sihukest põle" : Inimene.Leia("49009205216").Nimi.ToUpper() // peavad sama tüüpi olema - stringid
                );

            // avaldis?? "asendus"          //colaesce tehe
            // avaldis == null ? "asendus" : avaldis

            string test = "*";
            while (test != "")
                
            try
            {
                Console.Write("Anna oma isikukood: ");
                if (Inimene.TryCreate2(Console.ReadLine(), out Inimene i))
                {
                    Console.Write("Ja sinu nimi: ");
                    i.Nimi = Console.ReadLine();
                }
                else Console.WriteLine("Vigane või korduv isikukood");
            }
            catch (Exception e) // püüab vea kinni ja töötleb seda
            {
                    Console.WriteLine($"Selle jätame lisamata, kuna {e.Message}");
            }

            foreach (var x in Inimene.Inimesed) Console.WriteLine(x.Nimi + ":" + x.IK + ":" + x.Sünniaeg.ToShortDateString());
        }
    }

    class Inimene
    {
        static Dictionary<string, Inimene> _Inimesed = new Dictionary<string, Inimene>();

        public static IEnumerable<Inimene> Inimesed => _Inimesed.Values;
        static int loendur;

        public int Nr { get; } = ++loendur;
        public string Nimi { get; set; }
        //public string IK {get; private set; } // ainult classi siseselt muudetav
        public readonly string IK; // ainult constructoris
        public DateTime Sünniaeg { get; private set; }

        //Inimene(string ik)
        //{
        //    IK = ik;
        //   if (!Inimesed.ContainsKey(ik))Inimesed.Add(ik, this); // siis kõik inimesed lähevad mul Dictionary
        //}

        //või kirjuta nii - lühem..
        // public Inimene(string ik) => IK = ik;

         Inimene(string ik, string nimi = "nime veel pole", DateTime datetime = new DateTime())    //overloaded constructor - kaks sama nimega asja
            // : this(ik) // konstruktorite sidumine C#
        {
            //this(ik); // konstruktorite sidumine Javas
            Nimi = nimi;
            IK = ik;
            _Inimesed.Add(ik, this);
            Sünniaeg = datetime;
        }

        public static Inimene Create(string ik, string nimi = "")
             => _Inimesed.ContainsKey(ik) ? _Inimesed[ik] : new Inimene(ik, nimi);
        //{
        //    if (Inimesed.ContainsKey(ik)) return Inimesed[ik];
        //    else return new Inimene(ik, nimi);

        // lühemalt saab eelmist asja kirjutada nii return asemel if, else

        //}

        public static Inimene Leia(string ik) => _Inimesed.ContainsKey(ik) ? _Inimesed[ik] : null;

        public static bool TryCreate(string ik, out Inimene inimene) // vastus on jah või ei - ja siis kirjutab välja kas andis inimese luua või ei..

            => (inimene = (ik.Length == 11 && !_Inimesed.ContainsKey(ik) && DateTime.TryParse(
                (ik[0] == '1' || ik[0] == '2' ? "18" :
                 ik[0] == '3' || ik[0] == '4' ? "19" :
                 ik[0] == '5' || ik[0] == '6' ? "20" : "xx") +
                 ik.Substring(1, 2) + "/" +
                 ik.Substring(3, 2) + "/" +
                 ik.Substring(5, 2)
                 , out DateTime d)) ? new Inimene(ik, "", d) : null) != null;

        public static bool TryCreate2(string ik, out Inimene inimene)
        {
            inimene = null;
            if (ik.Length == 11) throw new Exception("Isikukood vale pikkusega");
            if (_Inimesed.ContainsKey(ik)) throw new Exception($"Selline {ik} juba on");
            {
                string kp = "xx";

                switch (ik[0])
                {
                    case '1':
                    case '2':
                        kp = "18";
                        break;
                    case '3':
                    case '4':
                        kp = "19";
                        break;
                    case '5':
                    case '6':
                        kp = "20";
                        break;
                }

                kp += ik.Substring(1, 2) + "/" + ik.Substring(3, 2) + "/" + ik.Substring(5, 2);
                if (DateTime.TryParse(kp, out DateTime d))
                {
                    inimene = new Inimene(ik, "", d);
                    return true;
                }
                else throw new Exception($"");
            }
            // return false; kui else lause lõpus siis seda enam ei ole tarvis..
        
        }

    }

}
